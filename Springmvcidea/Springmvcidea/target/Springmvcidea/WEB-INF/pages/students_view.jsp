<%-- 
    Document   : students_view
    Created on : Jan 17, 2015, 2:10:59 PM
    Author     : ignac_000
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<h2>Studenci</h2>
<table class="table" style="margin-bottom: 0;">
    <thead>
    <tr>
        <th class="col-md-2">Numer albumu</th>
        <th class="col-md-2">Nazwisko, imie</th>
        <th class="col-md-2">Saldo</th>
        <th class="col-md-2">Numer konta </th>
    </tr>
    </thead>
</table>
<div class="long-list">
    <table class="table table-striped">
        <thead style="opacity: 0; height: 0px; margin: 0; padding: 0;">
        <tr style="opacity: 0; height: 0px; margin: 0; padding: 0;">
            <th class="col-md-2"></th>
            <th class="col-md-2"></th>
            <th class="col-md-1"></th>
            <th class="col-md-2"></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${students}" var="student">
            <tr>
                <td>${student.albumNumber}</td>
                <td>${student.name} ${student.surName}</td>                
                <td>${student.account.balance}</td>
                <td>${student.account.number}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

