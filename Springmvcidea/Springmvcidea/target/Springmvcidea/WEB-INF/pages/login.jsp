<%-- 
    Document   : login
    Created on : Jan 17, 2015, 3:09:09 PM
    Author     : ignac_000
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<f:view>
   <div class="container">
      <div class="row">
          <div class="col-md-offset-2 col-md-8 jumbotron text-center" style="padding-top: 10px;">
            <h1><span class='brand'>EKZOS</span> SGGW</h1>
            <form class="form-signin">
              
              <label for="inputEmail" class="sr-only">Adres email (login)</label>
              <input id="inputEmail" path="login" class="form-control" placeholder="Adres email (login)" autofocus="" type="email">

              <label for="inputPassword" class="sr-only">Haslo</label>
              <input id="inputPassword" path="password" class="form-control" placeholder="Haslo" type="password">
              
              <a href="dashboard.html">
                <button class="btn btn-lg btn-primary btn-block" value="login" type="submit" >Zaloguj sie</button>
              </a>
              <br/>
              
              <hr/>
              
              <span id="helpBlock" class="help-block">Nie pamietasz hasla? Podaj swoj email</span>
              <label for="inputRecover" class="sr-only">Nie pamietasz hasla? Podaj swoj email</label>
              <input id="inputRecover" class="form-control" value="remind" placeholder="Adres email" autofocus="" type="email" aria-describedby="helpBlock">
              <button class="btn btn-info btn-block">Resetuj haslo</button>

              <hr/>
              
              <span id="helpBlock" class="help-block">Nie jestes jeszcze zarejestrowany?</span>
              <label for="inputRecover" class="sr-only">Nie jestes jeszcze zarejestrowany?</label>
              <button class="btn btn-info btn-block" value="register">Zarejestruj sie!</button>
            </form>
          </div>
        </div>
      </div> <!-- /container -->
</f:view>
