package com.springapp.mvc;

import com.springapp.mvc.models.TransferViewModel;
import ekzos.model.Department.Department;
import ekzos.model.User.UserModel;
import ekzos.model.dao.implementation.UserDaoImpl;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Session;
import org.hibernate.Transaction;
import static org.springframework.http.RequestEntity.method;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@SessionAttributes
@RequestMapping("/transfer_clerk_single")
public class TransferSingleController
{

    private TransferViewModel regModel = new TransferViewModel();
    
    @ModelAttribute("operation")
    public TransferViewModel GetRegModel(){
        return new TransferViewModel();
    }
    
    
    @RequestMapping()
    public ModelAndView TransferSingle() {
        //Testing data only! TODO: change it to data get by parameter from context
        Set<UserModel> sum = new HashSet<UserModel>(){};

        UserModel activeUser = new UserModel();
        UserDaoImpl udi = new UserDaoImpl();
        try
        {
            Session s = udi.openCurrentSession();
            Transaction t = s.beginTransaction();
            activeUser = udi.findAll().get(0);
            
            for(Department d : activeUser.getDepartments())
            {
                for(UserModel um :d.getUserList() )
                {
                    sum.add(um);
                }
            }
            
            t.commit();
        }catch(Exception e)
        {
            udi.getSessionFactory().getCurrentSession().getTransaction().rollback();
        } finally{
            udi.closeSessionAndTransaction();
        }
        
        
        ModelAndView model = new ModelAndView("transfer_clerk_single");

        model.addObject("students", sum);
        model.addObject("user", activeUser);
        
        return model;
    }
    
    @RequestMapping(value = "realizeOperation", method = RequestMethod.GET)
    public String realizeOperation( @ModelAttribute("operation") TransferViewModel operation, BindingResult result) {
         
        System.out.println("Operacja przeprowadzona");
        
        return "redirect:/transfer_clerk_single";
        //return "redirect:/students.jsp";
    }
}
