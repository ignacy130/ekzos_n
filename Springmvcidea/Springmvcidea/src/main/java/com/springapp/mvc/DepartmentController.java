package com.springapp.mvc;

import ekzos.model.Department.Department;
import ekzos.model.User.UserModel;
import ekzos.model.dao.implementation.UserDaoImpl;
import java.util.HashSet;
import java.util.Set;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by ignac_000 on 17/1/2015.
 */

@Controller
@RequestMapping("/students")
public class DepartmentController
{
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView students() {
        //Testing data only! TODO: change it to data get by parameter from context
        Set<UserModel> sum = new HashSet<UserModel>(){};

        UserModel activeUser = new UserModel();
        UserDaoImpl udi = new UserDaoImpl();
        try
        {
            Session s = udi.openCurrentSession();
            Transaction t = s.beginTransaction();
            activeUser = udi.findAll().get(0);
            
            for(Department d : activeUser.getDepartments())
            {
                for(UserModel um :d.getUserList() )
                {
                    sum.add(um);
                }
            }
            
            t.commit();
        }catch(Exception e)
        {
            udi.getSessionFactory().getCurrentSession().getTransaction().rollback();
        } finally{
            udi.closeSessionAndTransaction();
        }
        
        ModelAndView model = new ModelAndView("students");
        model.addObject("students", sum);
        model.addObject("user", activeUser);
        return model;
    }
}
