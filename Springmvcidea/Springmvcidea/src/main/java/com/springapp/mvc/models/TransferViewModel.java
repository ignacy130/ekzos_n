/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapp.mvc.models;

import ekzos.model.Operation.FinancialOperation;
import ekzos.model.User.UserModel;
import java.math.BigDecimal;

/**
 *
 * @author Mateusz
 */
public class TransferViewModel{
	public String recieverName;
	public String recieverAddress;
	public BigDecimal amount;
        public String number;
        public Boolean isPlus;
        public String title;
        
        public String getRecieverName(){
            return recieverName;
        }
        public void setRecieverName(String val){
            recieverName = val;
        }
        
        public String getTitle(){
            return title;
        }
        public void setTitle(String val){
            title = val;
        }
        
        public String getRecieverAddress(){
            return recieverAddress;
        }
        public void setRecieverAddress(String val){
            recieverAddress = val;
        }
        
        public String getNumber(){
            return number;
        }
        public void setNumber(String val){
            number = val;
        }
        
        public BigDecimal getAmount(){
            return amount;
        }
        public void setAmount(BigDecimal val){
            amount = val;
        }

        public Boolean getIsPlus(){
            return isPlus;
        }
        public void setIsPlus(Boolean val){
            isPlus = val;
        }
	public TransferViewModel(){
		//crate objects and initialize fields
	}

}
