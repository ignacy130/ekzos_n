import ekzos.model.UserModel;
import ekzos.model.dao.implementation.UserDao.Implementation;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.lang.Exception;


@Controller
@RequestMapping("loginUser")
public class LoginController {

    @ModelAttribute
    public ModelAndView showLoginPage() {
        ModelAndView mav = new ModelAndView("login")
        return mav;
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public String loginMethod(@ModelAttribute("user") UserModel userToLogin, BindingResult result) {
        UserDaoImpl UserCorrect = new UserDaoImpl();
        try {

            Session s = UserCorrect.openCurrentSession();
            Transaction t = s.beginTransaction();
            List<UserdModel> users = UserCorrect.findAll();
            String email = userToLogin.getEmail();
            Boolean passwordOK = userToLogin.getPassword() != null &&
                    (userCorrect.findUserByEmail(userToLogin.getemail()).getPassword).equals(userToLogin.getPassword());
            String userType = userToLogin.getType();
            if (userType.equals("student")) {
                if (passwordOK) {

                    s.persist(userToLogin);
                    t.commit();
                    ModelAndView mav = new ModelAndView("students");
                    mav.addObject("students", userToLogin);
                }
                ModelAndView mav2 = new ModelAndView("login");
            } else if (userType.equals("clerk")) {
                if (passwordOK){
                    s.persist(userToLogin);
                    t.commit();
                }

            }
        }
        catch (Exception e)
            USerCorrect.getSessionFactory().getCurrentSession.getTransaction().rollback();
     finally {
            UserCorrect.closeSessionAndTransaction();
        }
    }
}