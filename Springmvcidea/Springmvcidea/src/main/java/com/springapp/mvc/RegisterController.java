import  ekzos.model.UserModel;
import ekzos.model.dao.implementation.UserDao.Implementation;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;



@Controller
@RequestMapping("/register");
publc class RegisterController{

    @ModelAttribute
    @RequestMapping(value = "registerPage", method = RequestMethod.GET)
    public ModelAndView showRegisterPage(){

        ModelAndView mav = new ModelAndView("register");
        return mav;
    }

    @RequestMapping(value="send", method =RequestMethod.POST)
    public String send(@ModelAttribute("user") UserModel userReg, BindingResult result){

        UserDaoImpl udi = new UserDaoImpl();
        try {
            Session s = udi.openCurrentSession();
            Transaction t = s.beginTransaction();
            List<UserModel> users = udi.findAll();
            String email = regUser.getEmail();
            Boolean isNew = !users.stream().anyMatch(x -> x.getEmail().equals(email));
            Boolean passwordOK = regUser.getPassword() != null && regUser.getPassword().isEmpty();
            Boolean albumOK = regUser.getAlbumNumber().length() == 6;
            Boolean depOK = !regUser.getDepartments().isEmpty();

            if (isNew && passwordOK && albumOK && depOK) {
                s.persist(regUser);
                t.commit();

                ModelAndView model = new ModelAndView("success");
                model.addObject("user", regUser);
                return "redirect:register_success.html";
            }

        } catch (Exception e) {
            udi.getSessionFactory().getCurrentSession().getTransaction().rollback();
        } finally {
            udi.closeSessionAndTransaction();
        }
        return "redirect:registration_failed.html";
    }

}