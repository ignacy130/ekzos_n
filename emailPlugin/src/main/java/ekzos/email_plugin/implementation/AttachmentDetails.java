package ekzos.email_plugin.implementation;

import com.sun.xml.internal.messaging.saaj.packaging.mime.internet.MimeBodyPart;

/**
 * Created by Mateusz on 2014-11-24.
 */
public class AttachmentDetails {
    public String fileName;
    public javax.mail.internet.MimeBodyPart attachmentBody;
}
