package ekzos.email_plugin.implementation;

import ekzos.email_plugin.contract.ICommunicator;

import java.util.*;
import java.util.List;

import javax.mail.*;
//import javax.mail.MessagingException;
import javax.mail.internet.*;
import javax.activation.*;

/**
 * Created by Mateusz on 2014-11-24.
 */
public class EmailHelper implements ICommunicator
{
    private static class Holder {
        static final EmailHelper INSTANCE = new EmailHelper();
    }

    public static EmailHelper getInstance() {
        return Holder.INSTANCE;
    }

    /**
     * Creates an email.
     *
     * @param senderAddress   - Sender address
     * @param recipientAddress - Receiver address
     * @param title           - Email title
     * @param textOfMessage   - Email content
     * @param attachmentList  - List of email attachments
     */
    @Override
    public MimeMessage createEmail(String senderAddress, String recipientAddress, String title, String textOfMessage, List<AttachmentDetails> attachmentList) throws MessagingException, javax.mail.MessagingException {
        String host = "localhost";
        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", host);
        Session session = Session.getDefaultInstance(properties);

        //From - To
        MimeMessage message = new MimeMessage(session);
        message.setFrom((new InternetAddress(senderAddress)));
        message.setSender(new InternetAddress(senderAddress));
        message.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(recipientAddress));

        //Title and body
        message.setSubject(title);
        BodyPart messageBodyPart = new javax.mail.internet.MimeBodyPart();
        messageBodyPart.setText(textOfMessage);

        //Creating multipart message
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);

        //Adding attachments
        if (attachmentList != null && !attachmentList.isEmpty())
        {
            for (AttachmentDetails at : attachmentList)
            {
                DataSource ds = new FileDataSource(at.fileName);
                at.attachmentBody.setDataHandler(new DataHandler(ds));

                at.attachmentBody.setFileName(at.fileName);
                multipart.addBodyPart(at.attachmentBody);
            }
        }

        //Sending
        message.setContent(multipart);
        return message;
    }

    /**
     * Creates and sends an email.
     *
     * @param senderAddress   - Sender address
     * @param receiverAddress - Receiver address
     * @param title           - Email title
     * @param textOfMessage   - Email content
     * @param attachmentList  - List of email attachments
     */




    public void sendEmail(String senderAddress, String receiverAddress, String title, String textOfMessage, List<AttachmentDetails> attachmentList) throws javax.mail.MessagingException {
        Message message;

        try
        {
            message = createEmail(senderAddress, receiverAddress, title, textOfMessage, attachmentList);
            Transport.send(message);
        }
        catch (MessagingException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Sends an email.
     *
     * @param message - message to be send
     */
    public void sendEmail(MimeMessage message) throws javax.mail.MessagingException {
        try
        {
            Transport.send(message);
        }
        catch (javax.mail.MessagingException e)
        {
            e.printStackTrace();
        }
    }


    public void sendRecoveryLink(String recipientAddress, String senderAddress, String message)
    {

        MimeMessage email = null;
        try
        {
            email = createEmail(senderAddress, recipientAddress, "Odzyskaj hasło", message, null);
            this.sendEmail(email);
        }
        catch (MessagingException e)
        {
            e.printStackTrace();
        }
    }
}
