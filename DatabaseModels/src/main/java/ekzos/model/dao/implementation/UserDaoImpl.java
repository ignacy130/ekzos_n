package ekzos.model.dao.implementation;



import java.util.ArrayList;
import java.util.List;

import ekzos.model.dao.contract.UserDao;
import org.springframework.stereotype.Repository;
import ekzos.model.User.UserModel;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Paweł on 2015-01-03.
 */
@Repository
public class UserDaoImpl extends BaseDao<UserModel> implements UserDao{


    @Override
    @SuppressWarnings("unchecked")
    @Transactional
    public UserModel findByUserName(String username) {
        List<UserModel> users = new ArrayList<UserModel>();

        users = openCurrentSession()
                .createQuery("from UserModel where username=?")
                .setParameter(0, username)
                .list();

        if (users.size() > 0) {
            return users.get(0);
        } else {
            return new UserModel();
        }

    }

    @Override
    public Class<?> type() {
        return UserModel.class;
    }
}
