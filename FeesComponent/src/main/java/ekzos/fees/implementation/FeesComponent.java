package ekzos.fees.implementation;

import ekzos.fees.contract.IFeesComponent;
import ekzos.model.Operation.FinancialOperation;
import ekzos.model.Operation.ObligationOperation;
import ekzos.model.Operation.PeriodType;
import ekzos.model.User.AccountBank;
import ekzos.model.User.UserModel;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ignac_000 on 10/12/2014.
 */

public class FeesComponent implements IFeesComponent
{

    @Override
    public List<FinancialOperation> getOperations(UserModel user)
    {
        return user.getAccount().getFinancialOperations();
    }

    @Override
    public List<Date> getMaturities(UserModel user)
    {
        List<Date> dates = new ArrayList<Date>();
        List<FinancialOperation> ops = this.getOperations(user);
        for (FinancialOperation op : ops)
        {
            dates.add(op.getDate());
        }
        return dates;
    }

    @Override
    public List<BigDecimal> getFinancesValues(UserModel user)
    {
        List<BigDecimal> values = new ArrayList<BigDecimal>();
        List<FinancialOperation> ops = this.getOperations(user);
        for (FinancialOperation op : ops)
        {
            values.add(op.getAmount());
        }
        return values;
    }

    @Override
    public List<BigDecimal> getInterestValues(UserModel user)
    {
        List<BigDecimal> values = new ArrayList<>();
        List<FinancialOperation> ops = this.getOperations(user);
        ops.removeIf(x -> !(x instanceof ObligationOperation));
        for (FinancialOperation op : ops)
        {
            values.add(((ObligationOperation)op).getInterestValue());
        }
        return values;
    }

    @Override
    public void sendMoney(AccountBank account, BigDecimal amount)
    {
        account.setBalance(account.getBalance().add(amount));
    }

    @Override
    public void setDue(UserModel user, BigDecimal amount, BigDecimal interestRate, PeriodType periodType, String title, Date maturity)
    {
        AccountBank account = user.getAccount();
        account.getFinancialOperations().add(new ObligationOperation(new Date(), amount, title, account, interestRate, periodType, maturity));
    }

    @Override
    public void cancelDue(FinancialOperation operation)
    {
        //TODO relation and way of cancelling
        throw new NotImplementedException();
    }

}
